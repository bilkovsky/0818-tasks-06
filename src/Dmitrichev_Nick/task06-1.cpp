/*
������������ ��������� ���������� ��� �������� ��������� �������,
� �������� ������� ������� ������ ����, � ��������� ����������� 
����������� ����� ��������� �������� � ��������� ����� ������
*/
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "task06-1.h"//����������� ������ Circle
int main()
{
	float Pr_Con = 1000.0, Pr_Fen = 2000.0, Rad = 3.0, Wid_Pa = 1.0;//��� ����� �������� ��������� �������� ������
	float Cost = 0.0;
	int Rub, Kop;
	Circle Pool;
	Circle Pool_Big;
	Pool.setRad(Rad);
	Pool_Big.setRad(Rad + Wid_Pa);
	Cost = (Pool_Big.getAre() - Pool.getAre())*Pr_Con+Pool_Big.getFer()*Pr_Fen;
	Rub = (int)(Cost);//���������� Cost �� ����� � �������
	Kop = (int)((Cost - Rub) * 100);
	if ((Cost - Rub - Kop / 100) > 0.0)//���������� � ������� �������, ���� ������ �� ����� �����
		Kop++;
	printf("The cost of materials for the pool: %d rubles %d kopeks\n", Rub,Kop);
	return 0;
}
