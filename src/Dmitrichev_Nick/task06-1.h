#pragma once
#define pi 3.1415926535897932384626433832795
#define Er1 "Incorrect value"
using namespace std;
class Circle
{
private:
	double Radius, Ference, Area;
public:
	Circle()
	{
		Radius = 0;
		Ference = 0;
		Area = 0;
	}
	void setRad(double Rad)
	{
		if (Rad < 0)
			cout << Er1 << endl;
		else
		{
			Radius = Rad;
			Ference = 2 * pi*Radius;
			Area = Radius*Radius*pi;
		}
	}
	void setFer(double Fer)
	{
		if (Fer < 0)
			cout << Er1 << endl;
		else
		{
			Ference = Fer;
			Radius = Ference / (2 * pi);
			Area = Radius*Radius*pi;
		}
	}
	void setAre(double Are)
	{
		if (Are < 0)
			cout << Er1<<endl;
		else
		{
			Area = Are;
			Radius = sqrt(Area / pi);
			Ference = 2 * pi*Radius;
		}
	}
	double getRad()
	{
		return Radius;
	}
	double getFer()
	{
		return Ference;
	}
	double getAre()
	{
		return Area;
	}
	void getCircle(double &Rad, double &Fer, double &Are)
	{
		Rad = Radius;
		Fer = Ference;
		Are = Area;
	}
	~Circle()
	{
	}
};
