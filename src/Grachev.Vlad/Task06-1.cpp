#include "Taks06-1.h"
#include <iostream>
#include <iomanip>
using namespace  std;

int main()
{
	Circle LtlCircl, BgCircl;
	double result, r1 = 3.0, r2 = 4.0;
	LtlCircl.setRad(r1);
	BgCircl.setRad(r2);
	result = (BgCircl.getArea() - LtlCircl.getArea()) * 1000.0 + BgCircl.getFer() * 2000.0;
	cout << "Result: ";
	cout.width(8);
	cout << fixed << setprecision(2) << result << " rub\n";
	return 0;
}