/*Программа, подсчитывающая стоимость дорожки и забора вокруг бассейна*/
#include <iostream>
#include "task06-1.h"

using namespace std;

int main()
{	
	double price_track, price_wall;
	Circle pool(3.0);
	Circle wall(4.0);
	
	price_track = (wall.get_area() - pool.get_area()) * 1000;	
	price_wall = wall.get_ference() * 2000;
		
	cout << "Price track: " << price_track << " rub." << endl;
	cout << "Price wall: " << price_wall << " rub." << endl;	
	return 0;
}
