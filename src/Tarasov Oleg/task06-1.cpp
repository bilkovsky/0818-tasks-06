#include <iostream>
#include "Circle.h"
#include <locale.h>
using namespace std;

#define CONCRETE_COST 1000		// 1 �� ���� ��������� ��������
#define FENCE_COST 2000			// 1 �������� ���� ������
#define POOL_RADIUS 3.0
#define PATH_WIDTH 1.0

double GetCost(double length, double maxArea, double minArea) {
	return length*FENCE_COST + (maxArea - minArea)*CONCRETE_COST;
}

int main() {
	setlocale(LC_ALL, "Rus");
	Circle Pool(POOL_RADIUS);
	Circle PoolPath(POOL_RADIUS + PATH_WIDTH);

	double cost = GetCost(Pool.GetRadius() + PoolPath.GetRadius(), PoolPath.GetArea(), Pool.GetArea());
	cout << "��������� ��������� �������� � ������: "<< cost << " ������." << endl;
	return 0;
}