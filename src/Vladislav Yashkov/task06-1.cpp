#include <iostream>
#include "task06-1.h"
#include <locale.h>
using namespace std;
#define POOL_RAD  3.0		
#define TRACK_WIDTH 1.0		
#define COST_TRACK 1000.0
#define COST_FENCE 2000.0
int main() {
	double sum = 0;
	Circle circle(POOL_RAD);
	double area_in = circle.getArea();
	circle.setRadius(TRACK_WIDTH + POOL_RAD);
	double area_out = circle.getArea();
	double track_area = area_out - area_in;
	double area_fence = circle.getFerence();
	sum = track_area*COST_TRACK + area_fence*COST_FENCE;
	printf("Sum: %.2f rub.\n", sum);
	return 0;
}