#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
using namespace std;
class Circle {
private:
	double Radius;
	double Ference;
	double Area;
public:
	Circle(double _rad) {
		setRadius(_rad);
	}
	void setRadius(double _rad) {
		Radius = _rad;
		Ference = 2 * Radius*M_PI;
		Area = M_PI*Radius*Radius;
	}
	void setFerence(double _ference) {
		Ference = _ference;
		Radius = Ference/2/M_PI;
		Area = M_PI*Radius*Radius;
	}
	void setArea(double _area) {
		Area = _area;
		Radius = sqrt(Area/M_PI);
		Ference = 2 * Radius*M_PI;
	}
	double getRadius() {
		return Radius;
	}
	double getFerence() {
		return Ference;
	}
	double getArea() {
		return Area;
	}
};