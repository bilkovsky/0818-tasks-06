#include <iostream>
#include "task06-1.h"
#define CostOf1sqmConcrete	1000
#define CostOf1mFence		2000
#define PoolRadius		3
#define WayWidth		1
#include <conio.h>
int main()
{
	double Cost;
	Circle Pool, PoolWithWay;

	Pool.setRadius(PoolRadius);
	PoolWithWay.setRadius(PoolRadius + WayWidth);

	Cost = (PoolWithWay.getArea() - Pool.getArea()) * CostOf1sqmConcrete + PoolWithWay.getArea() * CostOf1mFence;
	printf("Material cost is %0.2f (rubles.copecks)",Cost);
	getch();
	return 0;
}